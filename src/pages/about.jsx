import React from 'react';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import { Header } from 'components';
import { Layout, Container } from 'layouts';

const About = center => (
  <Layout>
    <Helmet title={'About Page'} />
    <Header title="About Me"></Header>
    <Container center={center}>
      <h3>
        A software developer.
        You can find me on {' '}
        <a target="_blank" href="https://linkedin.com/in/the-nathaniel-bromhead">LinkedIn</a>
      </h3>
    </Container>
  </Layout>
);

export default About;

About.propTypes = {
  center: PropTypes.object,
};
