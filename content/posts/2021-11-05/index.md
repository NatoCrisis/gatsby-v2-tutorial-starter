---
path: "/learning-aws-through-impractical-projects"
cover: "../images/fourtyeight.jpg"
date: "2021-11-05"
title: "Learning AWS Through Impractical Projects"
published: true
tags: [AWS]
---

As a software developer, staying relevant in your career will require you learn new technologies that may not come up in your day-to-day duties. Cloud technologies are becoming
increasingly popular, but not every company has caught up. I know from personal experience how difficult it can be to look for new work if your current employer is behind the curve, and
you haven't built up a resume that makes you competitive.

Side projects are a great way to cover the gap, but to be honest, there are only so many blogs, Twitter clones, and Instagram knock offs you can build before
the self-directed learning process get stale.

>**Note:** In this series of posts about learning AWS, my intention is not to teach any basic programming skills. Hopefully, you will have some grasp of at least one 
> programming language, because I will be jumping around between Python and Java.

So let us get our hands dirty and learn what Amazon Web Services has to offer by building something else:

![Markov](markov.jpg)

What is this? A project I've had on my mind for a while, a [Markov generator](https://en.wikipedia.org/wiki/Markov_chain) 
for the Marvel Cinematic Universe. Is this practical? Not in the conventional sense; the end product is good for a chuckle at best, but the process of building it
can teach you more than 10 Tinder imitators.

Of course, this can be achieved quite simply with a basic Django app or something similar, but our goal here is to learn AWS. Therefore, I am going to 
over-engineer this project to make the most of the learning experience. 
![Diagram](aws-diagram.png)

You don't need to understand this architecture diagram, each piece will be explained as we go.

Let me outline the next series of posts that details the moving parts:
1. Generating Markov text: With the help of the [markovify](https://github.com/jsvine/markovify) library, I will show how this can be done using AWS Lambda
2. Placing the text on an image: Again, using AWS Lambda.
3. Keeping it private: Creating a Private API Gateway endpoint for the new Lambda's.
4. Putting it all together: Deploy a Spring Boot app on EC2 that can create a faux MCU scene like the one above.

I will walk through each of these steps in detail, link to the code I have deployed, and hopefully you will be inspired to build your own impractical project.

---

And this is just the beginning. My plan is to further enhance this project by creating an entire workflow to upload new characters with a user-friendly interface, and to extend it even 
further by allowing even further properties to be markovified.
