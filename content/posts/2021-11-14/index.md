---
path: "/markovify-proof-of-concept"
cover: "../images/one.jpg"
date: "2021-11-14"
title: "Markovify Proof of Concept"
published: true
tags: ['Python','markovify']
---

Before we start deploying any code to AWS, lets start with a simple proof-of-concept to see how the [markovify](https://github.com/jsvine/markovify) library works.

>I will be using Python 3.6.9 for this proof-of-concept (POC). If you choose to follow along, and need help installing Python on your system, I suggest heading over to [The Hitchhiker's Guide to Python](https://docs.python-guide.org/). 
> In addition, I tend to run my Python samples via command line, **bash** in this case, so you don't need to worry about setting up an IDE at this point.

You have likely seen Markov text generators in action somewhere on the internet. The general idea is to take real text, and generate a random sentence that *sort of* appears to have come
from the original text or speaker. You can find plenty of Twitter bots that are likely using a [Markov chain](https://en.wikipedia.org/wiki/Markov_chain) to generate funny tweets, 
sometimes [impersonating a character](https://twitter.com/bot_homer), or a [real person](https://twitter.com/mar_phil_bot).

With the markovify library, we don't have to worry about how a Markov chain works, we can get one up and running very simply.

### 1. Install markovify

Of course, you will need to have markovify installed in order to use it.

`pip install markovify`

### 2. Project setup

No fancy setup will be required for this POC (one of the nice things about Python), so I will just be using one file called `main.py`. 
We will also need some raw text to use, which I have [included](files/raw.txt). It is just a small sample of dialogue spoken by Thor, so the final output will likely hew very close
to the actual dialogue from the films. The greater sample of text you have, the better the output from markovify, but that is not important right now.

Create a directory called `markov-poc`. In that directory create a file called `main.py` and put the `raw.txt` file next to it. You should have the following setup:
``` 
markov-poc
  |   main.py
  |   raw.txt
```

### 3. Markovify in action
The most basic usage of markovify is to read the raw text, and build a Markov model from it.
```javascript
import markovify

 f = open('raw.txt', 'r')
 raw_text = f.read()
 model = markovify.Text(raw_text)
 f.close()

 for i in range(10):
   print(model.make_sentence())
```

Finally, run our file with the command `python main.py`, and you should see 10 lines of output
> example output: **See this, this was aged for a walk, and uh...I always wear this.**

You will also see `None` as output. Don't worry about this, the markovify library will sometimes return `None` when it cannot reasonably construct a sentence. We will deal with this later.

### 4. Extending our POC
Step three was a good starting place. When putting together a POC, don't try to do everything at once. Start small and build up from there. 

The next step is to break this down even further. Why? I don't want to jump too far ahead, but I already know that when we go to deploy our AWS Lambda function, it would be very
inefficient to read a large file, build the Markov model, and return the output. Fortunatly, markovify allows us to build the model and save that to its own file that can be read
separately.
```javascript
import markovify

 # Read in the raw text to create our model from
 f = open('raw.txt', 'r')
 raw_text = f.read()
 f.close()

 # Create a JSON object from the Markov model
 model = markovify.Text(raw_text)
 model_json = model.to_json()

 # Write the JSON to a file
 f = open('raw_model.json', 'w')
 f.write(model_json)
 f.close()

 # Open the JSON file
 f = open('raw_model.json', 'r')
 j = f.read()
 f.close()

 markov_gen = markovify.Text.from_json(j)
 for i in range(10):
   print(markov_gen.make_sentence())
```
Just like before, run `python main.py` and we should see some output like this:
> I feel your pain.I need a hammer, not a stone, someone called it a stone before.

### 5. Next steps
Now that we have a basic understanding of how the markovify library works, we can forge ahead and deploy some code to AWS Lambda. In the next
post I will walk you through a very simple setup, and you will have your own AWS Lambda to generate Markov text.

