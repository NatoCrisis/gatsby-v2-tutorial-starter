---
path: "/using-markovify-in-aws-lambda"
cover: "../2018-10-15/blocks.jpg"
date: "2021-11-17"
title: "Using markovify in AWS Lambda"
published: false
tags: ['AWS Lambda','AWS SAM', 'markovify', 'Python']
---
In this post, I'm going to walk through how you can use the [markovify](https://github.com/jsvine/markovify) library in AWS Lambda, including:
1. Introduce you to AWS Serverless Application Model (SAM).
2. How to deploy Lambda layers.

If you don't know what [AWS Lambda](https://aws.amazon.com/lambda/) is, head on over to AWS and read up on the documentation. Amazon provides all the features you will find,
including use cases, whitepapers, reference architectures, and a helpful [Hello, World!](https://aws.amazon.com/getting-started/hands-on/run-serverless-code/) tutorial to get you started.
If you have never used Lambda, I recommend going through the tutorial because I'm going to dive right in.

## 1. Introduction to SAM

The [AWS Serverless Application Model](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/what-is-sam.html) is a framework created by AWS to simplify 
creating Lambda functions. In the past, I found developing for Lambda to be a frustrating experience due to the available toolset, whereas SAM is a breath of fresh air. With SAM, I
was able to:
1. Easily code a Lambda function.
2. Test my code locally.
3. Easily deploy my code, along with any additional resources like API Gateway.

> As before, I'm not going to walk through most of the installation process for tools that we use. AWS has plenty of documentation to get you started, head over to 
> [Installing the AWS SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html) for the specific instructions for
> your OS.

### Set up a Markov project
We will start by creating a new SAM app. The SAM CLI is quite straight forward, but I will walk you through it.

1. Initialize a new project
```
sam init
 Which template source would you like to use?
	1 - AWS Quick Start Templates
	2 - Custom Template Location
Choice: 1
 What package type would you like to use?
	1 - Zip (artifact is a zip uploaded to S3)	
	2 - Image (artifact is an image uploaded to an ECR image repository)
Package type: 1
Which runtime would you like to use?
	1 - nodejs14.x
	2 - python3.9
	3 - ruby2.7
	4 - go1.x
	5 - java11
	6 - dotnetcore3.1
	7 - nodejs12.x
	8 - nodejs10.x
	9 - python3.8
	10 - python3.7
	11 - python3.6
	12 - python2.7
	13 - ruby2.5
	14 - java8.al2
	15 - java8
	16 - dotnetcore2.1
Runtime: 11
Project name [sam-app]: markovify-function
AWS quick start application templates:
	1 - Hello World Example
	2 - EventBridge Hello World
	3 - EventBridge App from scratch (100+ Event Schemas)
	4 - Step Functions Sample App (Stock Trader)
Template selection: 1
```
The SAM CLI will generate a new project called markovify-function will the following directory structure:
```
markovify-function/
 |--events/
 |  |-event.json
 |--hello_world/
 |  |-__init__.py
 |  |-app.py
 |  |-requirements.txt
 |--tests/
 |  |-unit/
 |    |-__init__.py
 |    |-test_handler.py
 |--__init__.py
 |--.gitignore
 |--__init__.py
 |--README.md
 |--template.yaml
```
### Test our new function
We should make sure that our project is working properly with a quick test. SAM provides tools to run a function locally, 
so lets start by building the project with `sam build`. We can run the project with `sam local start-api` which will run the API 
locally on port 3000. We can then use `curl` to invoke our function

`curl http://localhost:3000/hello`

> You may notice that the README file says you can invoke the function with curl http://localhost:3000/. This is a bit 
> misleading, as you will need the path /hello when you use the out-of-the-box setup, I will explain why later.

The response you get from the curl command should be the following:

`{"message": "hello world"}`

### Add our markovify code

Open up the file app.py, in the hello_world directory. This is the entry point to our function; all requests will first be passed to
the function `lambda_handler(event, context)`. This file will have auto-generated comments, which I will remove for the purpose of 
brevity, and you should have this:

```coffeescript
import json

 def lambda_handler(event, context):

    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": "hello world"
        }),
    }
```

Take note of the `return`. When we tested the fuction via `curl`, this is why we received the message that we did.

Lets now update this function to return some Markov text, using what we learned from our proof-of-concept.

```coffeescript
import json
 import markovify

 def lambda_handler(event, context):

    # Wait...where is this coming from?
    f = open('raw_model.json', 'r')
    json_model = f.read()
    f.close()
    
    markov_generator = markovify.Text.from_json(json_model)
    to_return = None
    
    while to_return is None:
      to_return = markov_generator.make_sentence()
    
    return {
        "statusCode": 200,
        "body": json.dumps({
            "markov_text": to_return
        }),
    }
```

Now let us test it again with the same `curl` command as before:

`{"message":"Internal server error"}`

Uh oh....

## 2. Lambda Layers

Ok, I have to admit I lead you down the wrong path. This function will not work for two separate reasons.
1. `import markovify` does not work because our project is unable to access the markovify library.
2. `open('raw_model.json', 'r')` does not work because we have no Markov model to read.

One way to include the markovify library is to add it to the **requirements.txt** file (SAM should have generated the file with `requests` 
already added to the file):

```text
requests
 markovify
```
Then run `sam build` and the markovify library will be added and usable in your project.

But in the spirit of learning via over-engineering, we are going to use [AWS Lambda Layers](https://docs.aws.amazon.com/lambda/latest/dg/configuration-layers.html)
to deploy both the markovify library, and pre-generated Markov models (in JSON format).

### Manually creating Lambda Layers





## 3. Deploying the Function

### A quick work on IAM




