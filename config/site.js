module.exports = {
  pathPrefix: '/', // Prefix for all links. If you deploy your site to example.com/portfolio your pathPrefix should be "portfolio"
  title: 'Nathaniel Bromhead Site', // Navigation and Site Title
  titleAlt: 'Nathaniel Bromhead Site', // Title for JSONLD
  description: 'Nathaniel Bromhead website and blog',
  url: 'https://nathaniel-bromhead.com', // Domain of your site. No trailing slash!
  siteUrl: 'https://nathaniel-bromhead.com', // url + pathPrefix
  siteLanguage: 'en', // Language Tag on <html> element
  logo: 'static/logo/logo.png', // Used for SEO
  banner: 'static/logo/banner.png',
  // JSONLD / Manifest
  favicon: 'static/logo/favicon.png', // Used for manifest favicon generation
  shortName: 'nb-blog', // shortname for manifest. MUST be shorter than 12 characters
  author: 'Nathaniel Bromhead', // Author for schemaORGJSONLD
  themeColor: '#3e7bf2',
  backgroundColor: '#d3e0ff',
};
